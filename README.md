## Local Development

Create a .env file with the following:

```
ACCESS_TOKEN=<personal-access-token>
```

Install dependencies

`$ npm install`

Run 

`$ node index`

## Installing Node

Either of these are good choices for managing node versions (similar to rvm).

[Fast and Simple Node Version Manager](https://github.com/Schniz/fnm)

[Node Version Manager](https://github.com/nvm-sh/nvm/blob/master/README.md)