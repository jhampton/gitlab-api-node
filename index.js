'use strict';

// topics to cover
// I/O, memory, async execution, streams

// == create a .env 
// ACCESS_TOKEN=<personal-access-token>

require('dotenv').config();
const axios = require('axios');

// set defaults
axios.defaults.baseURL = 'https://gitlab.com/api/v4';
axios.defaults.headers.common['Private-Token'] = process.env.ACCESS_TOKEN;

const projectsUri = '/projects';
const pipelineUri = '/projects/:id/pipelines';

const getProjectPipeline = projectId => {
  axios
    .get(pipelineUri.replace(':id', projectId))
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.error(error);
    });
};

const getProjects = () => {
  axios
    .get(projectsUri)
    .then(function (response) {
      console.log(`== found ${response.data.length} projects`);
      return getProjectPipeline(response.data[0].id);
    })
    .catch(function (error) {
      console.error(error);
    });
};

const getProject = () => {
  axios
    .get('/projects/7764')
    .then(function (response) {
      return getProjectPipeline(response.data.id);
    })
    .catch(function (error) {
      console.error(error);
    });
};

getProject();
